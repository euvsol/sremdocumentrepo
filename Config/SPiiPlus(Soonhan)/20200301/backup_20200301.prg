#/ Controller version = 2.70
#/ Date = 3/1/2020 6:10 PM
#/ User remarks = 
#1
! Y(LOWER) 1 AXIS 
GLOBAL REAL UDCOM
GLOBAL INT HomeFlag(64)
GLOBAL INT Y_HOMEDONE
REAL Save(2)(5), TimeOut
REAL Search_Vel, Commut_current
REAL Home_Offset
INT AXIS,AXIS_L
INT HomingBuffer

! 1 *************************************************************************
! Set user variable initialize 
Home_Offset = -138							! Home offset value
AXIS = 1									! axis number
AXIS_L = 3
HomingBuffer = 0
TimeOut = 1000               				! [1sec], TimeOut depends on Homing time: serch vel and travel Time out 
Search_Vel = 20								! home search velrocity
Commut_current = XRMS(AXIS)*0.4        		! Commut Current < XRMS
HomeFlag(AXIS) = -1							! home flag reset
HomeFlag(UPPER) = -1						! home flag reset
! *************************************************************************
! Laser feedback switch disable
setsp(0,getspa(0,"axes[1].laser_sw.all"),0); WAIT 10
setsp(0,getspa(0,"axes[1].sb_mode"),0); WAIT 10
IF PST(21).#RUN
	STOP 21
END
! *************************************************************************

! 2 *************************************************************************
! -.servoboost off 
setsp(0,getspa(0,"axes[1].SLSBORD"),0)		! To turn off  set 0
setconf(251,1,102)							! To turn off set 102
MFLAGS(AXIS).30	= 0
MFLAGS(AXIS).31	= 0
! 3 **************************************************************************
! -.Set parameters for motor flags
DISABLE(AXIS)
WAIT 1000
TILL ^MST(AXIS).#ENABLED

SAFETYGROUP(AXIS)				! safetygroup disable
Save(AXIS)(0)=VEL(AXIS)			! operating motion profile backup
Save(AXIS)(1)=ACC(AXIS)
Save(AXIS)(2)=DEC(AXIS)
Save(AXIS)(3)=JERK(AXIS)
Save(AXIS)(4)=KDEC(AXIS)

MFLAGS(AXIS).#DEFCON = 1			! Connect function disable[error mapping or inputshaping]
MFLAGS(AXIS).#OPEN = 0			! close loop enable
!*****************************************************************************
CERRI(AXIS) = 1
CERRA(AXIS) = 1
CERRV(AXIS) = 1

! 4 *************************************************************************
! -.Set Motion parameters for homing 
VEL(AXIS) = Search_Vel
ACC(AXIS)= VEL(AXIS)*40!30
DEC(AXIS)=ACC(AXIS)
JERK(AXIS) = ACC(AXIS)*40
KDEC(AXIS) = JERK(AXIS)
! 5***************************************************************************
! -.Disable the default response of the hardware and software limits
FMASK(AXIS).#SRL=0
FMASK(AXIS).#SLL=0
FDEF(AXIS).#SRL=0
FDEF(AXIS).#SLL=0 
FDEF(AXIS).#LL=0
FDEF(AXIS).#RL=0 
WAIT 100

E_TYPE(AXIS) = 3
LOOP 2
FCLEAR AXIS
WAIT 500
END
E_TYPE(AXIS) = 4
LOOP 2
FCLEAR AXIS
WAIT 500
SET FPOS(AXIS) = 0
END
WAIT 100
! 6 **************************************************************************
! -.Commutation for each motor
IF MFLAGS(AXIS).#HALL = 0
	IF MFLAGS(AXIS).#BRUSHOK=0
	    SLPKP (AXIS)=150
		SLVKP(AXIS)= 70
		MFLAGS(AXIS).9=0				! Commutation reset
		ENABLE (AXIS)
		TILL MST(AXIS).#ENABLED
		WAIT 500
		COMMUT (AXIS), Commut_current		! Encoder commutation command
		TILL MFLAGS(AXIS).9, (TimeOut*10)		
		IF ^MFLAGS(AXIS).9
  			DISP"Axis commutation fault"
  			GOTO Time_Out
		END
	SLPKP (AXIS)=120
	SLVKP(AXIS)=50
	DISABLE (AXIS)
	WAIT 100
	DISP" Axis commutation comp"
	END
END

! 7 **************************************************************************
! -.Start homing - look for left limit switch
DISP" Axis Home Start, Move to search limit"
ENABLE (AXIS) 
TILL MST(AXIS).#ENABLED 
JOG (AXIS), -                                           ! Move to left limit  at low speed  
!TILL ABS(PE(AXIS)) >=0.005*CERRV(AXIS);
!HALT AXIS;
!DISABLE AXIS
!WAIT 500
!JOG(AXIS)
TILL FAULT(AXIS).#LL, (TimeOut * 100)       ! TimeOut 60sec 
IF ^FAULT(AXIS).#LL
  DISP" Axis move to search limits fault "
  GOTO Time_Out
END
HALT (AXIS)
TILL ^MST(AXIS).#MOVE
WAIT 100
!ENABLE (AXIS)
!WAIT 500
DISP" Axis move to search limit comp, Axis move to search index "
IST(AXIS).#IND=1      ! Prepare for finding indexes 
WAIT 10
IST(AXIS).#IND=0      ! Prepare for finding indexes 
WAIT 10

JOG/v (AXIS), Search_Vel/2 ! Move to index  at low speed  
TILL IST(AXIS).#IND,(TimeOut * 40) 
IF ^ IST(AXIS).#IND
  DISP" Axis move to search index fault " 
  GOTO Time_Out
END
HALT (AXIS)
TILL ^MST(AXIS).#MOVE; WAIT 500
DISP" Axis move to search index comp "

IF(E_TYPE(AXIS) = 4)
! -.Original homing procedure here
! -.Move to a middle of a quadrant, close to the index
! -.location
DISP"FPOS(AXIS)",FPOS(AXIS)
PTP/E (AXIS),IND(AXIS)+ POW(2,(E_SCMUL(AXIS)-3))*EFAC(AXIS)
TILL ^MST(AXIS).#MOVE
WAIT 100
! -.Repeatability correction
SET FPOS(AXIS) = FPOS(AXIS) - IND(AXIS) - GETCONF(265,AXIS)

ELSE
	SET FPOS(AXIS) = FPOS(AXIS) - IND(AXIS)
END
!*****************************************************************************

! 8 **************************************************************************
! -.Move to zero position
PTP/E AXIS, 0
TILL ^MST(AXIS).#MOVE
WAIT 100
!*****************************************************************************

! 9 **************************************************************************
! -.Move to Home_Offset
PTP/E AXIS, Home_Offset
TILL ^MST(AXIS).#MOVE
WAIT 500
SET FPOS(AXIS) = 0
SET FPOS(AXIS_L) = 0
WAIT 100
PTP/E AXIS, 0
TILL ^MST(AXIS).#MOVE
WAIT 100
!*****************************************************************************

! 10 *************************************************************************
! -.Set software limits and unmask left/right limit faults
FMASK(AXIS).#RL=0
FMASK(AXIS).#LL=1 
!FMASK(AXIS).#SRL=1
!FMASK(AXIS).#SLL=1
!FDEF(AXIS).#SRL=0
!FDEF(AXIS).#SLL=0; 
FDEF(AXIS).#LL=1
FDEF(AXIS).#RL=0      
!*****************************************************************************

!  11 ************************************************************************
! -. Y Mapping Program
! -. Version: 1.0
! -. Date: 2012.12.22
! -. MMI Version: NT Version	
! -. Sample reference axis : upper
GLOBAL REAL MAP_Y(11)	! "10" mapping 10 point.
! -.Y2 axis : MAP_X
! -.Z axis : MAP_Z, etc.....
MAP_Y(0) = 0.0 /1000
MAP_Y(1) = -4.4 /1000
MAP_Y(2) = -8.7 /1000
MAP_Y(3) = -12.9 /1000
MAP_Y(4) = -17.7 /1000
MAP_Y(5) = -22.9 /1000
MAP_Y(6) = -27.2 /1000
MAP_Y(7) = -31.4 /1000
MAP_Y(8) = -35.5 /1000
MAP_Y(9) = -40.2 /1000
MAP_Y(10) = -44.0 /1000

!MFLAGS(1).#DEFCON =0
! -.MFLAGS( Axis name ) 
! -.Axis name : 0, 1, 2, 3, 4, 5, 6, 7, 8, ......
!connect RPOS(1)= APOS(1) - MAP(APOS(1), MAP_Y, 0, 35) 
! -.RPOS( Axis_name), ! APOS( Axis_name)
! -.MAP( APOS(Axis_name), MAP Error Array variable, Measure Start Position, Measure Inetval Position)
! -.Map Error Array variable : MAP_X
! -.Start Positon : 0
! -.Measure Interval Positon : 10
!depends 1, 1 
! -.Depends Axis_name, Axis_name
WAIT 100

!  12 ************************************************************************
! -.servoboost parameters for tuning only

!  13 ************************************************************************
! -.Set home done flags and encoder filter 
HomeFlag(AXIS)=1
Y_HOMEDONE = 1
DISP" Axis Home Done = , Axis number=",HomeFlag(AXIS), AXIS
!SET FPOS(AXIS+2) = FPOS(AXIS)
!*****************************************************************************

START 30, 1	! SERVO BOOST PLUS ENABLE
! *************************************************************************
! Laser feedback switch parameter
IF ^PST(10).#RUN
	START 10,1
END
! Laser Encoder Reset program
!IF ^PST(11).#RUN
!	START 11,1
!END
! *************************************************************************

! 14 **************************************************************************
! -.Restore previous motion parameters
Restore:
VEL(AXIS)=Save(AXIS)(0)
ACC(AXIS)=Save(AXIS)(1)
DEC(AXIS)=Save(AXIS)(2)
JERK(AXIS)=Save(AXIS)(3)
KDEC(AXIS) = Save(AXIS)(4)
!*****************************************************************************
STOP

! 15 *************************************************************************
Time_Out:
DISP" Home fault time_out, Axis number=",AXIS
HALT (AXIS)
TILL ^MST(AXIS).#MOVE, TimeOut
DISABLE (AXIS)
GOTO Restore
STOP
!*****************************************************************************

! 16 *************************************************************************
! -.Set software limits and unmask left/right limit faults
ON ^PST(1).#RUN
SRLIMIT(1)=351
SLLIMIT(1)=-0.1
FMASK(1).#RL=0
FMASK(1).#LL=1; 
FMASK(1).#SRL=1
FMASK(1).#SLL=1
FDEF(1).#SRL=1
FDEF(1).#SLL=1
FDEF(1).#LL=1
FDEF(1).#RL=0  
FDEF(1).#CPE=1
DISP" Axis safety all enable, Axis Nomber = 1"
RET
!*****************************************************************************

#2
! X(UPPER) 0 AXIS 
GLOBAL REAL UDCOM
GLOBAL INT HomeFlag(64)
GLOBAL INT X_HOMEDONE
REAL Save(1)(5), TimeOut
REAL Search_Vel, Commut_current
REAL Home_Offset
INT AXIS
INT HomingBuffer


! 1 *************************************************************************
! Set user variable initialize 
Home_Offset = -44				! Home offset value
AXIS = 0							! axis number
HomingBuffer = 0
TimeOut = 1000               		! [1sec], TimeOut depends on Homing time: serch vel and travel Time out 
Search_Vel = 20						! home search velrocity
Commut_current = XRMS(AXIS)*0.6     ! Commut Current < XRMS
HomeFlag(AXIS) = -1					! home flag reset
HomeFlag(LOWER) = -1				! home flag reset

! *************************************************************************
! Laser feedback switch disable
setsp(0,getspa(0,"axes[0].laser_sw.all"),0); WAIT 10
setsp(0,getspa(0,"axes[0].sb_mode"),0); WAIT 10
IF PST(20).#RUN
	STOP 20
END

! *************************************************************************

! 2 *************************************************************************
! -.servoboost off 
setsp(0,getspa(0,"axes[0].SLSBORD"),0) 		! To turn off  set 0
setconf(251,0,102)! To turn off set 102
MFLAGS(AXIS).30	= 0
MFLAGS(AXIS).31	= 0
! 3 **************************************************************************
! -.Set parameters for motor flags
DISABLE(AXIS)
WAIT 1000
TILL ^MST(AXIS).#ENABLED

SAFETYGROUP(AXIS)				! safetygroup disable
Save(AXIS)(0)=VEL(AXIS)			! operating motion profile backup
Save(AXIS)(1)=ACC(AXIS)
Save(AXIS)(2)=DEC(AXIS)
Save(AXIS)(3)=JERK(AXIS)
Save(AXIS)(4)=KDEC(AXIS)

MFLAGS(AXIS).#DEFCON = 1			! Connect function disable[error mapping or inputshaping]
MFLAGS(AXIS).#OPEN = 0			! close loop enable
!*****************************************************************************
CERRI(AXIS) = 1
CERRA(AXIS) = 1
CERRV(AXIS) = 1

! 4 *************************************************************************
! -.Set Motion parameters for homing 
VEL(AXIS) = Search_Vel
ACC(AXIS)= VEL(AXIS)*40
DEC(AXIS)=ACC(AXIS)
JERK(AXIS) = ACC(AXIS)*40
KDEC(AXIS) = JERK(AXIS)

! 5***************************************************************************
! -.Disable the default response of the hardware and software limits

FMASK(AXIS).#SRL=0
FMASK(AXIS).#SLL=0
FDEF(AXIS).#SRL=0
FDEF(AXIS).#SLL=0 
FDEF(AXIS).#LL=0
FDEF(AXIS).#RL=0 
WAIT 100

E_TYPE(AXIS) = 3
LOOP 2
FCLEAR AXIS
WAIT 500
END
E_TYPE(AXIS) = 4
LOOP 2
FCLEAR AXIS
WAIT 500
SET FPOS(AXIS) = 0
END
WAIT 100
! 6 **************************************************************************
! -.Commutation for each motor
IF MFLAGS(AXIS).#HALL = 0
	IF MFLAGS(AXIS).#BRUSHOK=0
		MFLAGS(AXIS).9=0				! Commutation reset
		ENABLE (AXIS)
		TILL MST(AXIS).#ENABLED
		WAIT 500
		COMMUT (AXIS), Commut_current		! Encoder commutation command
		TILL MFLAGS(AXIS).9, (TimeOut*10)		
		IF ^MFLAGS(AXIS).9
  			DISP"Axis commutation fault"
  			GOTO Time_Out
		END
	DISABLE (AXIS)
	WAIT 100
	DISP" Axis commutation comp"
	END
END

! 7 **************************************************************************
! -.Start homing - look for left limit switch
DISP" Axis Home Start, Move to search limit"
ENABLE (AXIS) 
TILL MST(AXIS).#ENABLED 
JOG (AXIS), -                                           ! Move to left limit  at low speed  
!TILL ABS(PE(AXIS)) >=0.005*CERRV(AXIS);JOG(AXIS)
TILL FAULT(AXIS).#LL, (TimeOut * 100)       ! TimeOut 60sec 
IF ^FAULT(AXIS).#LL
  DISP" Axis move to search limits fault "
  GOTO Time_Out
END
HALT (AXIS)
TILL ^MST(AXIS).#MOVE
WAIT 100

DISP" Axis move to search limit comp, Axis move to search index "
IST(AXIS).#IND=1      ! Prepare for finding indexes 
WAIT 10
IST(AXIS).#IND=0      ! Prepare for finding indexes 
WAIT 10
JOG/v (AXIS), Search_Vel ! Move to index  at low speed  
TILL IST(AXIS).#IND,(TimeOut * 40) 
IF ^ IST(AXIS).#IND
  DISP" Axis move to search index fault " 
  GOTO Time_Out
END
HALT (AXIS)
TILL ^MST(AXIS).#MOVE; WAIT 500
DISP" Axis move to search index comp "


IF(E_TYPE(AXIS) = 4)
! -.Original homing procedure here
! -.Move to a middle of a quadrant, close to the index
! -.location
PTP/E (AXIS),IND(AXIS)+ POW(2,(E_SCMUL(AXIS)-3))*EFAC(AXIS)
TILL ^MST(AXIS).#MOVE
WAIT 100
! -.Repeatability correction
SET FPOS(AXIS) = FPOS(AXIS) - IND(AXIS) - GETCONF(265,AXIS)

ELSE
	SET FPOS(AXIS) = FPOS(AXIS) - IND(AXIS)
END
!*****************************************************************************

! 8 **************************************************************************
! -.Move to zero position
PTP/E AXIS, 0
TILL ^MST(AXIS).#MOVE
WAIT 100
!*****************************************************************************

! 9 **************************************************************************
! -.Move to Home_Offset
PTP/E AXIS, Home_Offset
TILL ^MST(AXIS).#MOVE
WAIT 100
SET FPOS(AXIS) = 0
WAIT 100
PTP/E AXIS, 0
TILL ^MST(AXIS).#MOVE
WAIT 100
!*****************************************************************************

! 10 *************************************************************************
! -.Set software limits and unmask left/right limit faults
!FMASK(AXIS).#RL=1
!FMASK(AXIS).#LL=1 
FMASK(AXIS).#SRL=1
FMASK(AXIS).#SLL=1
FDEF(AXIS).#SRL=0
FDEF(AXIS).#SLL=0; 
!FDEF(AXIS).#LL=1
!FDEF(AXIS).#RL=1      
!*****************************************************************************

! 11 *****************************************************************************
! -. X Mapping Program
! -. Version: 1.0
! -. Date: 2012.12.22
! -. MMI Version: NT Version	
! -. Sample reference axis : upper
X_MAPPING:
GLOBAL REAL MAP_X(11)	! "10" mapping 10 point.
! -.Y2 axis : MAP_X
! -.Z axis : MAP_Z, etc.....

MAP_X(0) = -0.0 /1000
MAP_X(1) =  1.1 /1000
MAP_X(2) =  2.3 /1000
MAP_X(3) =  3.1 /1000
MAP_X(4) =  3.8 /1000
MAP_X(5) =  4.0 /1000
MAP_X(6) =  4.3 /1000
MAP_X(7) =  4.7 /1000
MAP_X(8) =  5.6 /1000
MAP_X(9) =  6.4 /1000
MAP_X(10) = 7.3 /1000

!MFLAGS(0).#DEFCON =0
! -.MFLAGS( Axis name ) 
! -.Axis name : 0, 1, 2, 3, 4, 5, 6, 7, 8, ......
!connect RPOS(0)= APOS(0) - MAP(APOS(0), MAP_X, 0, 18) 
! -.RPOS( Axis_name), ! APOS( Axis_name)
! -.MAP( APOS(Axis_name), MAP Error Array variable, Measure Start Position, Measure Inetval Position)
! -.Map Error Array variable : MAP_X
! -.Start Positon : 0
! -.Measure Interval Positon : 10
!depends 0, 0
! -.Depends Axis_name, Axis_name
wait 100

!  12 ************************************************************************
! -.servoboost parameters for tuning only

!  13 ************************************************************************
! -.Set home done flags and encoder filter 
HomeFlag(AXIS)=1

X_HOMEDONE = 1
DISP" Axis Home Done = , Axis number=",HomeFlag(AXIS), AXIS
!SET FPOS(AXIS+2) = FPOS(AXIS)

START 29, 1	! SERVO BOOST PLUS ENABLE
! *************************************************************************
! Laser feedback switch parameter
IF ^PST(10).#RUN
	START 10,1
END

! *************************************************************************

! 14 **************************************************************************
! -.Restore previous motion parameters
Restore:
VEL(AXIS)=Save(AXIS)(0)
ACC(AXIS)=Save(AXIS)(1)
DEC(AXIS)=Save(AXIS)(2)
JERK(AXIS)=Save(AXIS)(3)
KDEC(AXIS) = Save(AXIS)(4)
!*****************************************************************************
STOP

! 15 *************************************************************************
Time_Out:
DISP" Home fault time_out, Axis number=",AXIS
HALT (AXIS)
TILL ^MST(AXIS).#MOVE, TimeOut
DISABLE (AXIS)
GOTO Restore
STOP
!*****************************************************************************

! 16 *************************************************************************
! -.Set software limits and unmask left/right limit faults
ON ^PST(2).#RUN
SRLIMIT(0)=181
SLLIMIT(0)=-0.1
FMASK(0).#RL=1
FMASK(0).#LL=1 
FMASK(0).#SRL=1
FMASK(0).#SLL=1
FDEF(0).#SRL=1
FDEF(0).#SLL=1 
FDEF(0).#LL=1
FDEF(0).#RL=1  
FDEF(0).#CPE=1
DISP" Axis safety all enable, Axis Nomber = 0"
RET
!*****************************************************************************


#3
!X AXIS XL80 & LINEAR SCALE MAPPING DATA 

!MAP_X(0) = 0.0 /1000
!MAP_X(1) = 0.7 /1000
!MAP_X(2) = 1.7 /1000
!MAP_X(3) = 2.4 /1000
!MAP_X(4) = 3.2 /1000
!MAP_X(5) = 3.5 /1000
!MAP_X(6) = 3.6 /1000
!MAP_X(7) = 3.9 /1000
!MAP_X(8) = 4.1 /1000
!MAP_X(9) = 4.3 /1000
!MAP_X(10) = 4.8 /1000

!X AXIS RLD LASER ENCODER MAPPING DATA

!MAP_X(0) = -0.0 /1000
!MAP_X(1) = -0.1 /1000
!MAP_X(2) = -0 /1000
!MAP_X(3) = -0.3 /1000
!MAP_X(4) = -0.7 /1000
!MAP_X(5) = -1.1 /1000
!MAP_X(6) = -1.6 /1000
!MAP_X(7) = -2.1 /1000
!MAP_X(8) = -2.3 /1000
!MAP_X(9) = -2.6 /1000
!MAP_X(10) = -2.7 /1000


!Y AXIS XL80 & LINEAR SCALE MAPPING DATA 

!MAP_Y(0) = 0.0 /1000
!MAP_Y(1) = 0.3 /1000
!MAP_Y(2) = 0.2 /1000
!MAP_Y(3) = -0.4 /1000
!MAP_Y(4) = -1.9 /1000
!MAP_Y(5) = -3.9 /1000
!MAP_Y(6) = -5.1 /1000
!MAP_Y(7) = -6.4 /1000
!MAP_Y(8) = -8.1 /1000
!MAP_Y(9) = -9.9 /1000
!MAP_Y(10) = -10.8 /1000

!Y AXIS RLD LASER ENCODER MAPPING DATA

!MAP_Y(0) = 0.0 /1000
!MAP_Y(1) = -5.4 /1000
!MAP_Y(2) = -10.7 /1000
!MAP_Y(3) = -15.5 /1000
!MAP_Y(4) = -21.2 /1000
!MAP_Y(5) = -27.1 /1000
!MAP_Y(6) = -32.0 /1000
!MAP_Y(7) = -36.5 /1000
!MAP_Y(8) = -41.4 /1000
!MAP_Y(9) = -46.7 /1000
!MAP_Y(10) = -51.3 /1000

#4
SLIKP(1)=250!450
SLIKI(1)=2000!5500

SLPKP(1)=100
SLVKP(1)=100
SLVKI(1)=200
SLVSOF(1)=1300

SLPKPIF(1)=1!1.5
SLVKPIF(1)=1!1.5
SLVKIIF(1)=1

SLPKPSF(1)=1!2.2
SLVKPSF(1)=1!2.2
SLVKISF(1)=1!2.2

DELI(1)=500

STOP


SLIKP(0)=300!400
SLIKI(0)=4000!7500

SLPKP(0)=100
SLVKP(0)=100
SLVKI(0)=300

SLPKPIF(0)=1.5
SLVKPIF(0)=1.5
SLVKIIF(0)=1.5

SLPKPSF(0)=1
SLVKPSF(0)=1.7
SLVKISF(0)=1.5

DELI(0)=100

STOP

#10
GLOBAL INT HomeFlag(64)
!*******************************************
! Calculates raw feedbacks
int ADD(8)
ADD(0)= ECGETOFFSET("HSSI 1",0)
ADD(1)= ECGETOFFSET("HSSI 2",0)
ADD(2)= ECGETOFFSET("HSSI 3",0)
ADD(3)= ECGETOFFSET("HSSI 4",0)
ADD(4)= ECGETOFFSET("HSSI 5",0)
ADD(5)= ECGETOFFSET("HSSI 6",0)
ADD(6)= ECGETOFFSET("HSSI 7",0)
ADD(7)= ECGETOFFSET("HSSI 8",0)
ECIN(ADD(0),HSSI0)
ECIN(ADD(1),HSSI1)
ECIN(ADD(2),HSSI2)
ECIN(ADD(3),HSSI3)
ECIN(ADD(4),HSSI4)
ECIN(ADD(5),HSSI5)
ECIN(ADD(6),HSSI6)
ECIN(ADD(7),HSSI7)

TILL HomeFlag(LOWER) = 1 & HomeFlag(UPPER) = 1
WAIT 100

global real pos_raw(2),vel_raw(2)
GLOBAL INT TEST,LASER_MODE(2)
int SW
! Raw feedback calculation
real temp
! 4 feedbacks: 	2  encoders ENC_POS(0), ENC_POS(1)
!				2  lasers LASER_POS(0), LASER_POS(1)
! The following command replaces HSSI information in the telegram with feedbacks. 
FCLEAR 0;FCLEAR 1;FCLEAR 2;FCLEAR 3
V1=0
ENC_POS(0) = FPOS(0); ENC_POS(1) = FPOS(1);pos_raw(0) = (HSSI0&0XFFFF)+POW(2,16)*HSSI1 ; while 1;block
! The program is doing extention to the 16bit feedbacks

if (^FAULT0.#ENC) & (^FAULT0.#ENCNC)
	temp = (HSSI0&0XFFFF)+POW(2,16)*HSSI1 
	vel_raw(0)= (temp - pos_raw(0));
	if vel_raw(0)>=pow(2,31);vel_raw(0)= vel_raw(0)-pow(2,32);END
	if vel_raw(0)<=-pow(2,31); vel_raw(0) = vel_raw(0)+pow(2,32);END
	pos_raw(0)= temp;

	ENC_POS(0) = ENC_POS(0) + vel_raw(0)*EFAC(0);
	IF V1 = 0 ENC_POS(0)=FPOS(0) END
end
if (^FAULT1.#ENC) & (^FAULT1.#ENCNC)
	temp = (HSSI2&0XFFFF)+POW(2,16)*HSSI3 
	vel_raw(1)= (temp - pos_raw(1));
	if vel_raw(1)>=pow(2,31);vel_raw(1)= vel_raw(1)-pow(2,32);END
	if vel_raw(1)<=-pow(2,31); vel_raw(1) = vel_raw(1)+pow(2,32);END
	pos_raw(1)= temp;
	ENC_POS(1) = ENC_POS(1) + vel_raw(1)*EFAC(1);
	IF V1 = 0 ENC_POS(1)=FPOS(1) END
end
V1=1
LASER_MODE(0)=HSSI4&0X0F
LASER_MODE(1)=(HSSI4&0XF0)/16

end;end
STOP

#19
SETSP(0,getspa(0,"axes[0].switch_gain"),0)
SETSP(0,getspa(0,"axes[1].switch_gain"),0)
STOP

#20
! UPPER LASER SWITCH MODE PROGRAM
GLOBAL REAL ULZ(4), LLZ(4)
GLOBAL INT LASER_MODE(2)
!INT UPPER, LOWER, FLAG(2)
INT FLAG(2)
INT ULaser, LLaser
REAL uOFFSET
! ULZ	: Upper Laser Zone
! LLZ	: Lower Laser Zone
! (0)	: upper low
! (1)	: upper high
! (2)	: lower low
! (3)	: lower high

!UPPER	= 0
!LOWER 	= 1
ULaser	= 2
LLaser	= 3
uOFFSET	= 5

FLAG(0)	= 0
FLAG(1)	= 0

! LASER RESET ZONE
ULZ(0) = 10!99		
ULZ(1) = 165!133
ULZ(2) = 30!50
ULZ(3) = 185!130

LLZ(0) = 30
LLZ(1) = 185
LLZ(2) = 10
LLZ(3) = 165
!TEST Y POS : 104~128 
!TEST X POS : 55~125

SETSP(0,getspa(0,"axes[0].switch_gain"),0)

WHILE 1
	! UPPER LASER ACTIVE ZONE
	IF ( RPOS(UPPER) >= ULZ(0) & RPOS(UPPER) <= ULZ(1) ) & ( RPOS(LOWER) >= ULZ(2) & RPOS(LOWER) <= ULZ(3) ) & FLAG(0) = 1
		!DISP"UPPER LASER ACTIVE ZONE"
		!=====================================================
		! Laser reset
		IF FAULT(ULaser).#ENCNC = 1
			DISP"Upper Laser Reset,FPOS(UPPER), FPOS(LOWER)", FPOS(0), FPOS(1)
			FCLEAR (ULaser)
			setsp(0,getspa(0,"axes[0].laser_sw.all"),0); WAIT 10
			!EFAC(ULaser)=3.86279E-008;WAIT 5
			!E_TYPE(ULaser) = 3; WAIT 5
			!FCLEAR (ULaser)
			!WAIT 5
			!E_TYPE(ULaser) = 4
			!FCLEAR (ULaser)
			WAIT 5
			SET FPOS(ULaser) = 0; WAIT 10
			!SETSP(0,GETSPA(0,"axes[2].ratio"),1.5822)
			!EFAC(ULaser)=EFAC(UPPER); XVEL(ULaser)=XVEL(ULaser)
			FCLEAR (UPPER); FCLEAR (ULaser)
			SET FPOS(ULaser) = FPOS(UPPER)
			WAIT 5
			
		END
		
		IF FAULT(ULaser).#ENCNC = 0
		!	DISP"Upper Laser Active"
		END
		!=====================================================
		
		!=====================================================
		! Dual loop switch routine
		IF ( RPOS(UPPER) >= (ULZ(0)+uOFFSET) & RPOS(UPPER) <= (ULZ(1)-uOFFSET) ) & ( RPOS(LOWER) >= (ULZ(2)+uOFFSET) & RPOS(LOWER) <= (ULZ(3)-uOFFSET) ) & FAULT(ULaser).#ENCNC = 0
			setsp(0,getspa(0,"axes[0].laser_sw.all"),1)
			!WAIT 10
			!FCLEAR (UPPER); FCLEAR(ULaser)
			SET FPOS(ULaser) = FPOS(UPPER)
			FLAG(0)	= 0
			DISP"Upper Laser Dual loop ON, FPOS(UPPER), FPOS(LOWER)", FPOS(UPPER), FPOS(LOWER)
		END
		!=====================================================
	END
	!=====================================================
	! Dual loop switch routine
	IF ( RPOS(UPPER) < (ULZ(0)+uOFFSET) | RPOS(UPPER) > (ULZ(1)-uOFFSET) ) | ( RPOS(LOWER) < (ULZ(2)+uOFFSET) | RPOS(LOWER) > (ULZ(3)-uOFFSET) ) & FLAG(0) = 0
		DISP" CALC ", FPOS(2) - ENC_POS(0)
		setsp(0,getspa(0,"axes[0].laser_sw.all"),0)
		FCLEAR 0; FCLEAR 2
		DISP"Upper Laser Dual loop OFF,FPOS(UPPER), FPOS(LOWER) ",  FPOS(UPPER), FPOS(LOWER)
		DISP"===================================="
		FLAG(0)	= 1
		
	END
	!=====================================================
END

STOP

ON ^PST(2).#RUN & LASER_MODE(UPPER) = 8
DISP"UPPER LASER MODE = , FAULT CLEAR", LASER_MODE(UPPER)
setsp(0,getspa(0,"axes[0].laser_sw.all"),0)
FCLEAR (UPPER)
RET

#21
! LOWER LASER SWITCH MODE PROGRAM
GLOBAL REAL ULZ(4), LLZ(4)
GLOBAL INT LASER_MODE(2)
!INT UPPER, LOWER
INT FLAG(2)
INT ULaser, LLaser
REAL uOFFSET
! ULZ	: Upper Laser Zone
! LLZ	: Lower Laser Zone
! (0)	: upper low
! (1)	: upper high
! (2)	: lower low
! (3)	: lower high

!UPPER	= 0
!LOWER 	= 1
ULaser	= 2
LLaser	= 3
uOFFSET	= 5!10

FLAG(0)	= 0
FLAG(1)	= 0

ULZ(0) = 10!0
ULZ(1) = 165!240
ULZ(2) = 30!116
ULZ(3) = 185!340

LLZ(0) = 10!70!160!129!10
LLZ(1) = 165!110!190!235
LLZ(2) = 30!165!70!0
LLZ(3) = 185!190!110!440
!TEST X POS : 134~158 
!TEST Y POS : 105~195 AXIS 1


SETSP(0,getspa(0,"axes[1].switch_gain"),0)

WHILE 1
	! LOWER LASER ACTIVE ZONE
	IF ( RPOS(UPPER) >= LLZ(0) & RPOS(UPPER) <= LLZ(1) ) & ( RPOS(LOWER) >= LLZ(2) & RPOS(LOWER) <= LLZ(3) ) & FLAG(1) = 1
		!DISP"LOWER LASER ACTIVE ZONE"
		!=====================================================
		! Laser reset
		IF FAULT(LLaser).#ENCNC = 1
			!STOP 10
			DISP"Lower Laser Reset", FPOS(0), FPOS(1)
			setsp(0,getspa(0,"axes[1].laser_sw.all"),0); WAIT 10
			!EFAC(LLaser)=3.86279E-008;WAIT 5
			!E_TYPE(LLaser) = 3; WAIT 5; 
			!FCLEAR (LLaser)
			!WAIT 5
			!E_TYPE(LLaser) = 4
			FCLEAR (LLaser)
			WAIT 5
			SET FPOS(LLaser) = 0; WAIT 10
			!SETSP(0,GETSPA(0,"axes[3].ratio"),1.5822)
			!EFAC(LLaser)=EFAC(LOWER); XVEL(LLaser)=XVEL(LLaser)
			FCLEAR (1); FCLEAR (3)
			SET FPOS(LLaser) = FPOS(LOWER)
			!WAIT 10
			!IF ^PST(10).#RUN
			!	START 10, 1
			!END
			!WAIT 100
			!FCLEAR ALL
		END
		
		IF FAULT(LLaser).#ENCNC = 0
		!	DISP"Lower Laser Active"
		END
		!=====================================================
		
		!=====================================================
		! Dual loop switch routine
		IF ( RPOS(UPPER) >= (LLZ(0)+uOFFSET) & RPOS(UPPER) <= (LLZ(1)-uOFFSET) ) & ( RPOS(LOWER) >= (LLZ(2)+uOFFSET) & RPOS(LOWER) <= (LLZ(3)-uOFFSET) ) & FAULT(LLaser).#ENCNC = 0
			setsp(0,getspa(0,"axes[1].laser_sw.all"),1)
			!WAIT 10
			FCLEAR (LOWER); FCLEAR(LLaser)
			SET FPOS(LLaser) = FPOS(LOWER)
			FLAG(1)	= 0
			DISP"Lower Laser Dual loop ON, FPOS(UPPER), FPOS(LOWER)", FPOS(UPPER), FPOS(LOWER)
		END
		!=====================================================
	END
	!=====================================================
	! Dual loop switch routine
	IF ( RPOS(UPPER) < (LLZ(0)+uOFFSET) | RPOS(UPPER) > (LLZ(1)-uOFFSET) ) | ( RPOS(LOWER) < (LLZ(2)+uOFFSET) | RPOS(LOWER) > (LLZ(3)-uOFFSET) ) & FLAG(1) = 0
		setsp(0,getspa(0,"axes[1].laser_sw.all"),0)
		DISP"Lower Laser Dual loop OFF, FPOS(UPPER), FPOS(LOWER)",  FPOS(UPPER), FPOS(LOWER)
		DISP"===================================="
		FLAG(1)	= 1
		!STOP 10
	END
	!=====================================================
END

STOP

ON ^PST(1).#RUN & LASER_MODE(LOWER) = 8
DISP"LOWER LASER MODE = , FAULT CLEAR", LASER_MODE(LOWER)
setsp(0,getspa(0,"axes[1].laser_sw.all"),0)
FCLEAR (LOWER); STOP 10; WAIT 10
START 10, 1

RET


#22
! AGING PROGRAM
VEL(0)	= 50
VEL(1)	= 50
WAIT 10

WHILE 1

PTP UPPER, 0; PTP/E LOWER, 0
WAIT 3000
PTP UPPER, 30; PTP/E LOWER, 440
WAIT 3000
PTP UPPER, 0; PTP/E LOWER, 0
WAIT 3000
PTP UPPER, 240; PTP/E LOWER, 440
WAIT 3000

END
STOP

#23
! Feedback switching off
setsp(0,getspa(0,"axes[0].laser_sw.all"),0); WAIT 10
setsp(0,getspa(0,"axes[1].laser_sw.all"),0); WAIT 10
STOP

#24
! Feedback switching on
setsp(0,getspa(0,"axes[0].laser_sw.all"),1); WAIT 10
setsp(0,getspa(0,"axes[1].laser_sw.all"),1); WAIT 10
STOP

#29
! UPPER AXIS[0] SERVO BOOST PLUS & ENCODER SWITCH PARAMETER
GLOBAL INT TEST,LASER_MODE(2)
! LASER ENCODER AXIS[2]
setsp(0,getspa(0,"axes[0].Ubus"),50) 
setsp(0,getspa(0,"axes[0].Rm"),7.0)    ! MEASURED VALUE IS 4.6
setsp(0,getspa(0,"axes[0].Lm"),2.5)
setsp(0,getspa(0,"axes[0].Ip"),16)
setsp(0,getspa(0,"axes[0].Ke"),25.2)
setsp(0,getspa(0,"axes[0].Pitch"),30.48)
setsp(0,getspa(0,"axes[0].motor_type"),1)
!ServoBoost configuration
setsp(0,getspa(0,"axes[0].SLSBBW"),80)
setsp(0,getspa(0,"axes[0].SLSBF"),0)
setsp(0,getspa(0,"axes[0].SLSBORD"),1) 
!ServoBoost configuration
setsp(0,getspa(0,"axes[2].SLSBBW"),80)
setsp(0,getspa(0,"axes[2].SLSBF"),0)
setsp(0,getspa(0,"axes[2].SLSBORD"),1) 
SETSP(0,GETSPA(0,"axes[0].sb_mode"),0)
setsp(0,getspa(0,"axes[0].laser_sw.all"),0)
! Encoder resolution  0.4u/16384 
! Laser resolution 0.1582200um/4096
! ratio = (0.1582200/0.4)*(4096/16384) = ?
! The ratio matches laser and encoder resolutions
! Now axis 2 counts in same units like axis 0. So EFAC is same 
! EFAC is determined according to enc resolution. Laser feedback is scaled accordingly
! RATIO is negative if laser has opposite polarity
SETSP(0,GETSPA(0,"axes[2].ratio"),1.5822)
EFAC(2)=EFAC(0); XVEL(2)=XVEL(0)
SLAFF(0) = 0.87
SET FPOS(2) = APOS(0)
! set 0 to disable
wait 10 
!EFAC(2)=3.86279E-008
setconf(251,0,101)   
setconf(251,0,100)
setconf(251,2,100)
MFLAGS(0).30 = 1
MFLAGS(0).31 = 1
MFLAGS(2).30 = 1
MFLAGS(2).31 = 1
setsp(0,getspa(0,"axes[0].stop_command"),0.2)
SETSP(0,GETSPA(0,"axes[0].sb_mode"),1)
LASER_MODE(0) = 0
DISP"UPPER SERVO BOOST PLUS ON"
STOP



#30
! LOWER AXIS[1] SERVO BOOST PLUS & ENCODER SWITCH PARAMETER
! LASER ENCODER AXIS[3]
GLOBAL INT TEST,LASER_MODE(2)
setsp(0,getspa(0,"axes[1].Ubus"),100) 
setsp(0,getspa(0,"axes[1].Rm"),6.0)    ! MEASURED VALUE IS 4.6
setsp(0,getspa(0,"axes[1].Lm"),3.2)
setsp(0,getspa(0,"axes[1].Ip"),16)
setsp(0,getspa(0,"axes[1].Ke"),39.4)
setsp(0,getspa(0,"axes[1].Pitch"),30.48)
setsp(0,getspa(0,"axes[1].motor_type"),1)
!ServoBoost configuration
setsp(0,getspa(0,"axes[1].SLSBBW"),100)
setsp(0,getspa(0,"axes[1].SLSBF"),0)
setsp(0,getspa(0,"axes[1].SLSBORD"),1) 
!ServoBoost configuration
setsp(0,getspa(0,"axes[3].SLSBBW"),100)
setsp(0,getspa(0,"axes[3].SLSBF"),0)
setsp(0,getspa(0,"axes[3].SLSBORD"),1) 
SETSP(0,GETSPA(0,"axes[1].sb_mode"),0)
setsp(0,getspa(0,"axes[1].laser_sw.all"),0)
! Encoder resolution  0.4u/16384 
! Laser resolution 0.1582200um/4096
! ratio = (0.1582200/0.4)*(4096/16384) = ?
! The ratio matches laser and encoder resolutions
! Now axis 2 counts in same units like axis 0. So EFAC is same 
! EFAC is determined according to enc resolution. Laser feedback is scaled accordingly
! RATIO is negative if laser has opposite polarity
SETSP(0,GETSPA(0,"axes[3].ratio"),1.5822)
EFAC(3)=EFAC(1); XVEL(3)=XVEL(1)
SLAFF(1) = 1.7156; WAIT 10
SET FPOS(3) = FPOS(1)
WAIT 10
!EFAC(3)=3.86279E-008
! set 0 to disable
wait 10 
setconf(251,1,101)   
setconf(251,1,100)
setconf(251,3,100)
MFLAGS(1).30 = 1
MFLAGS(1).31 = 1
MFLAGS(3).30 = 1
MFLAGS(3).31 = 1
setsp(0,getspa(0,"axes[1].stop_command"),0.2)
SETSP(0,GETSPA(0,"axes[1].sb_mode"),1)
DISP"LOWER SERVO BOOST PLUS ON"
LASER_MODE(1) = 0
STOP

#31
! Encoder Switch program - TEST PROGRAM
GLOBAL INT TEST,LASER_MODE(2)
WHILE 1
DISP"=========================================="
TILL LASER_MODE(0) = 0 & RVEL(0) > 0 & ENC_POS(0) >= 50!APOS(0) >= 50
setsp(0,getspa(0,"axes[0].laser_sw.all"),1)
DISP"LASER FPOS(LASER), ENC_POS(SCALE), APOS(...)", FPOS(2), ENC_POS(0), APOS(0)

TILL LASER_MODE(0) = 1 & RVEL(0) > 0 & ENC_POS(0) >= 150!APOS(0) >= 150
setsp(0,getspa(0,"axes[0].laser_sw.all"),0)
DISP"SCALE FPOS(LASER), ENC_POS(SCALE), APOS(...)", FPOS(2), ENC_POS(0), APOS(0)

TILL LASER_MODE(0) = 0 & RVEL(0) < 0 & ENC_POS(0) <= 150!APOS(0) <= 150
setsp(0,getspa(0,"axes[0].laser_sw.all"),1)
DISP"LASER FPOS(LASER), ENC_POS(SCALE), APOS(...)", FPOS(2), ENC_POS(0), APOS(0)

TILL LASER_MODE(0) = 1 & RVEL(0) < 0 & ENC_POS(0) <= 50!APOS(0) <= 50
setsp(0,getspa(0,"axes[0].laser_sw.all"),0)
DISP"SCALE FPOS(LASER), ENC_POS(SCALE), APOS(...)", FPOS(2), ENC_POS(0), APOS(0)

END

#A
!axisdef X=0,Y=1,A=2,B=3,Z=4,T=5,C=6,D=7
!axisdef x=0,y=1,a=2,b=3,z=4,t=5,c=6,d=7
global int I(100),I0,I1,I2,I3,I4,I5,I6,I7,I8,I9,I90,I91,I92,I93,I94,I95,I96,I97,I98,I99
global real V(100),V0,V1,V2,V3,V4,V5,V6,V7,V8,V9,V90,V91,V92,V93,V94,V95,V96,V97,V98,V99
global real acs_xHome,acs_yHome,acs_xTarget,acs_yTarget;
global int acs_xDwell,acs_yDwell,acs_xEnable, acs_yEnable;
global int acs_moveMode, acs_homeLimit, acs_xHomeManual,acs_xHomePositive, acs_yHomeManual,acs_yHomePositive;

GLOBAL INT HSSI0,HSSI1,HSSI2,HSSI3,HSSI4,HSSI5,HSSI6,HSSI7
global real ENC_POS(2), LASER_POS(2), ENC_VEL(2), LASER_VEL(2)
!global real SLSBBW(4), SLSBF(4), SLSBORD(4)

axisdef UPPER=0, LOWER=1

